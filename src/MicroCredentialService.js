import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express'

class MicroCredentialService extends ApplicationService {
    /**
     * 
     * @param {ExpressDataApplication} app 
     */
    constructor(app) {
        super(app);
    }
}

export {
    MicroCredentialService
}