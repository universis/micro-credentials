import { DataObject, EdmMapping } from '@themost/data';

@EdmMapping.entityType('EducationalCredentialsValidateResult')
class EducationalCredentialsValidateResult extends DataObject {
    constructor() {
        super();
    }
}

export {
    EducationalCredentialsValidateResult
}